import authRoutes from "./authRoutes";
import sessionRoutes from "./sessionRoutes";
import messageRoutes from "./messageRoutes";
import userRoutes from "./userRoutes";
import {isAuthenticated, isAdmin} from "../middlewares/auth.verify.middleware";

export default app => {
  app.use("/login", authRoutes);
  app.use("/session", sessionRoutes);
  app.use("/messages", isAuthenticated, messageRoutes);
  app.use("/users", isAuthenticated, userRoutes);

  app.use("*", (req, res) => {
    res.send({
      "error": true,
      "message": "Page not found."
    });
  });
};
