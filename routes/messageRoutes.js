import {Router} from "express";
import messageService from "../services/messageService";

const router = Router();

router.get("/", (req, res, next) => {
    const response = messageService.getMessageList();
    res.status(200).send(response);
});

router.post("/", async (req, res, next) => {
    await messageService.create(req.body.message);
    const response = messageService.getMessageList();
    res.status(200).send(response);
});

router.put("/", (req, res, next) => {

});

router.delete("/", (req, res, next) => {
    const messageId = req.query.message_id;
    messageService.delete(messageId);
    const response = messageService.getMessageList();
    res.status(200).send(response);
});

export default router;