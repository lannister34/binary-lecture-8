import {Router} from "express";
import userService from "../services/userService";
import {isAdmin, isAuthenticated} from "../middlewares/auth.verify.middleware";

const router = Router();

router.get("/", isAuthenticated, (req, res, next) => {
    const response = userService.getUserList();
    res.status(200).send(response);
});

export default router;