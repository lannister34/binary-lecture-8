import {Router} from 'express';
import {loginDataValidation} from '../middlewares/login.validation.middleware';
import authService from '../services/authService';

const router = Router();

router.post('/', loginDataValidation, async (req, res, next) => {
    const response = await authService.login(req.body);
    res.status(200).send(response);
});

export default router;