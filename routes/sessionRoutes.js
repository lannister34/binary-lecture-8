import {Router} from "express";
import sessionService from "../services/sessionService";

const router = Router();

router.post("/compare", async (req, res, next) => {
    const {id_token} = req.body;
    const response = sessionService.compare(id_token);
    res.status(200).send(response);
});

export default router;