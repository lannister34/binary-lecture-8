const loginDataValidation = function(req, res, next) {
  const body = req.body;
  if (body.username && body.password) {
    next();
  } else {
    res.status(400).send({
      "error": true,
      "message": "All fields must be filled."
    });
  }
}

module.exports = {loginDataValidation};
