import sessionService from "../services/sessionService";
import userService from "../services/userService";

const isAuthenticated = function(req, res, next) {
    const id_token = req.query.id_token || req.body.id_token;
    const result = sessionService.compare(id_token);
    if (result.isExist && !result.error) {
        req.id_token = id_token;
        next();
    } else {
        res.status(200).send({
           error: true,
           message: "You need to log in."
        });
    }
}

const isAdmin = function(req, res, next) {
    const session = sessionService.findByToken(req.id_token);
    const user = userService.findById(session.userId);
    if (user.admin) {
        next();
    } else {
        res.status(200).send({
            error: true,
            message: "You haven\'t permission."
        });
    }
}

const setAdminOption = function(req, res, next) {
    req.isAdmin = () => {
        return req.user && req.user.admin;
    };
    next();
}

module.exports = { isAuthenticated, isAdmin };