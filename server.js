import express from 'express';
import http from 'http';
import routes from './routes';
import {STATIC_PATH, PORT} from './config/config';
import cors from 'cors';
import bodyParser from 'body-parser';

const app = express();
const httpServer = http.Server(app);

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(express.static(STATIC_PATH));
routes(app);

httpServer.listen(PORT, () => {
  console.log(`Listen server on port ${PORT}`);
});
