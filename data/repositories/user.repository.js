import BaseRepository from './base.repository';

class UserRepository extends BaseRepository {
  constructor() {
    super('/users');
  }
}

module.exports = new UserRepository();