import BaseRepository from './base.repository';

class UserRepository extends BaseRepository {
    constructor() {
        super('/messages');
    }
}

module.exports = new UserRepository();