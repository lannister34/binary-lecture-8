import dbAdapter from "../db/dbAdapter";
import idGenerator from "../../helpers/idGenerator";

class BaseRepository {
  constructor(collectionName) {
    this.collectionName = collectionName;
  }

  async generateId() {
    return idGenerator.generate();
  }

  getAll() {
    return dbAdapter.getData(this.collectionName);
  }

  getById(id) {
    const data = this.getAll();
    return data.find((obj) => id === obj.id);
  }

  getByField(field, value) {
    const data = this.getAll();
    return data.find((obj) => value === obj[field]);
  }

  async create(data) {
    data.id = await this.generateId();
    dbAdapter.push(this.collectionName + '[]', data, true);
    return data;
  }

  update(id, dataToUpdate) {
    const index = dbAdapter.getIndex(this.collectionName, id);
    const data = this.getById(id);
    const newData = Object.assign({}, data, dataToUpdate);
    dbAdapter.push(this.collectionName + `[${index}]`, newData);
    return newData;
  }

  delete(id) {
    const index = dbAdapter.getIndex(this.collectionName, id);
    dbAdapter.delete(this.collectionName + `[${index}]`);
    return true;
  }
}

module.exports = BaseRepository;