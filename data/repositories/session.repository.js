import BaseRepository from './base.repository';

class SessionRepository extends BaseRepository {
    constructor() {
        super('/sessions');
    }
}

module.exports = new SessionRepository();