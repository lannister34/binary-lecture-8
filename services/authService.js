import userService from "./userService";
import bcrypt from "bcrypt";
import sessionService from "./sessionService";

class AuthService {
    async login(userData) {
        const {password} = userData;
        let {username} = userData;

        username = username.toLowerCase();

        const data = userService.findByUsername(username);

        if (!data) {
            return {
                error: true,
                message: "Account with this username doesn't exist."
            }
        }

        const passwordMatch = await bcrypt.compare(password, data.password);

        if (!passwordMatch) {
            return {
                    error: true,
                    message: "Wrong password."
                }
        }

        const sessionToken = await sessionService.create(data.id);

        return {
            error: false,
            id_token: sessionToken,
            user: data
        }
    }
}

module.exports = new AuthService();