import messageRepository from "../data/repositories/message.repository";

class MessageService {
    getMessageList() {
        return messageRepository.getAll();
    }
    async create(message) {
        return await messageRepository.create(message);
    }
    delete(messageId) {
        try {
            return messageRepository.delete(messageId);
        } catch (e) {
            return false;
        }
    }
}

module.exports = new MessageService();