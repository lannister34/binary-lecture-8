import userRepository from '../data/repositories/user.repository';

class UserService {
  getUserList() {
    return userRepository.getAll();
  }

  findById(id) {
    try {
      return userRepository.getById(id);
    } catch (e) {
      return null;
    }
  }

  findByUsername(username) {
    try {
      return userRepository.getByField('username', username);
    } catch (e) {
      console.log(e);
      return null;
    }
  }
}

module.exports = new UserService();