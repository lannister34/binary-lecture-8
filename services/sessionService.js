import sessionRepository from "../data/repositories/session.repository";
import userRepository from "../data/repositories/user.repository";
import idGenerator from "../helpers/idGenerator";

class SessionService {
    async create(userId) {
        const sessionToken = await idGenerator.generate();
        const session = {
            userId: userId,
            id_token: sessionToken
        };
        await sessionRepository.create(session);
        return sessionToken;
    }

    compare(sessionToken) {
        const falseResponse = {
            error: false,
            isExist: false
        };
        if (!sessionToken) {
            return falseResponse;
        }
        const session = sessionRepository.getByField("id_token", sessionToken);
        if (!session) {
            return falseResponse;
        }
        const user = userRepository.getById(session.userId);
        if (!user) {
            return falseResponse;
        }
        return {
            error: false,
            isExist: true,
            user
        };
    }

    findByToken(id_token) {
        try {
            return sessionRepository.getByField("id_token", id_token);
        } catch (e) {
            return null;
        }
    }
}

module.exports = new SessionService();