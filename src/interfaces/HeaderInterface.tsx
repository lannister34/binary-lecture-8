import Header from "../containers/Header";
import React from "react";

interface HeaderProps {
    id: string,
    name: string,
    userCount: number,
    messageCount: number,
    lastMessageTime: string | null
}

interface HeaderState {

}

export {HeaderProps, HeaderState}