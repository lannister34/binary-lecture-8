interface User {
    "userId": string,
    "user": string,
    "avatar": string
}

export default User;