import Message from "../Message/MessageInterface";
import User from "./UserInterface";

export const UPDATE_USERS: string = "UPDATE_USERS";
export const GET_MESSAGES: string = "GET_MESSAGES";
export const SEND_MESSAGE: string = "SEND_MESSAGE";
export const EDIT_MESSAGE: string = "EDIT_MESSAGE";
export const DELETE_MESSAGE: string = "DELETE_MESSAGE";
export const SHOW_MODAL: string = "SHOW_MODAL";
export const HIDE_MODAL: string = "HIDE_MODAL";

interface UpdateUsersAction {
    type: typeof UPDATE_USERS,
    userList: Array<User>
}

interface GetMessagesAction {
    type: typeof GET_MESSAGES,
    messageList: Array<Message>
}

interface SendMessageAction {
    type: typeof SEND_MESSAGE,
    message: Message
}

interface EditMessageAction {
    type: typeof EDIT_MESSAGE,
    message: Message
}

interface deleteMessageAction {
    type: typeof DELETE_MESSAGE,
    messageId: string
}

interface showModal {
    type: typeof SHOW_MODAL,
    messageId: string,
    action: (messageId: string, text: string) => void,
    closeAction: () => void
}

interface hideModal {
    type: typeof SHOW_MODAL
}

export type ChatActionTypes =
    UpdateUsersAction
    | GetMessagesAction
    | SendMessageAction
    | EditMessageAction
    | deleteMessageAction
    | showModal
    | hideModal;