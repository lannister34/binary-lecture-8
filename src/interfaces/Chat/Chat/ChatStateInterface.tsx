import MessageProps from "../Message/MessagePropsInterface";
import User from "./UserInterface";
import ModalPropsInterface from "../Modal/ModalPropsInterface";

interface ChatState {
    currentUser: User | null,
    userList: Map<string, User>,
    userCount: number,
    messageList: Array<MessageProps>,
    messageCount: number,
    lastMessageTime: string | null,
    isGotData: boolean,
    modalData: ModalPropsInterface
}

export default ChatState;