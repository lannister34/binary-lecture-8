interface ChatProps {
    chatId: string,
    chatName: string
}

export default ChatProps;