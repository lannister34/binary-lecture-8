import {Message, MessageProps} from "./MessageInterface";
import {ChatActionTypes, User} from "./ChatInterface";

export interface MessageListProps {
    messageList: Array<MessageProps>,
    userList: Map<string, User>,
    currentUser: User | null,
    editMessage: (message: Message) => ChatActionTypes,
    deleteMessage: (token_id: string, messageId: string) => ChatActionTypes,
    showModal: (messageId: string, action: (messageId: string, text: string) => void, closeAction: () => void) => ChatActionTypes,
    hideModal: () => ChatActionTypes,
    id_token: string
}


export interface MessageListState {
    messageList: Array<Message>
}

