import React from "react";

interface ModalPropsInterface {
    isShown: boolean,
    title: string,
    value: string,
    action: (text: string) => void,
    closeAction: () => void
}

export default ModalPropsInterface;