import React from "react";

interface InputPropsInterface {
    name: "string",
    value: "string",
    type: "string"
}

export default InputPropsInterface;