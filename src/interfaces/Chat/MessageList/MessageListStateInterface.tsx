import Message from "../Message/MessageInterface";

interface MessageListState {
    messageList: Array<Message>
}

export default MessageListState;