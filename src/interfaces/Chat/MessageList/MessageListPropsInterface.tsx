import MessageProps from "../Message/MessagePropsInterface";
import User from "../Chat/UserInterface";

interface MessageListProps {
    messageList: Array<MessageProps>,
    currentUser: User | null
}

export default  MessageListProps;