import {Message} from "./MessageInterface";
import {MessageProps} from "./MessageInterface";
import {ModalPropsInterface} from "./ModalInterface";

export const FETCH_MESSAGES: any = "FETCH_MESSAGES";
export const SAVE_MESSAGES: any = "GET_MESSAGES";
export const SEND_MESSAGE: any = "SEND_MESSAGE";
export const EDIT_MESSAGE: any = "EDIT_MESSAGE";
export const DELETE_MESSAGE: any = "DELETE_MESSAGE";
export const SHOW_MODAL: any = "SHOW_MODAL";
export const HIDE_MODAL: any = "HIDE_MODAL";
export const FETCH_USERS: any = "FETCH_USERS";
export const SAVE_USERS: any = "SAVE_USERS";

export interface User {
    "id": string,
    "username": string,
    "avatar": string,
    "admin": boolean
}

export interface ChatProps {
    chat: ChatState,
    currentUser: User,
    id_token: string,
    fetchUsers: (id_token: string) => ChatActionTypes,
    fetchMessages: (id_token: string) => ChatActionTypes,
    saveMessages: (messageList: Array<Message>) => ChatActionTypes,
    sendMessage: (id_token: string, message: Message) => void,
    editMessage: (message: Message) => ChatActionTypes,
    deleteMessage: (id_token: string, messageId: string) => ChatActionTypes,
    showModal: (messageId: string, action: (messageId: string, text: string) => void, closeAction: () => void) => ChatActionTypes,
    hideModal: () => ChatActionTypes
}

export interface ChatState {
    currentUser: User | null,
    userList: Map<string, User>,
    userCount: number,
    messageList: Array<MessageProps>,
    messageCount: number,
    lastMessageTime: string | null,
    modalData: ModalPropsInterface
}

interface FetchMessagesAction {
    type: typeof FETCH_MESSAGES,
    id_token: string
}

interface SaveMessagesAction {
    type: typeof SAVE_MESSAGES,
    messageList: Array<Message>
}

interface SendMessageAction {
    type: typeof SEND_MESSAGE,
    id_token: string,
    message: Message
}

interface EditMessageAction {
    type: typeof EDIT_MESSAGE,
    message: Message
}

interface DeleteMessageAction {
    type: typeof DELETE_MESSAGE,
    id_token: string,
    messageId: string
}

interface ShowModalAction {
    type: typeof SHOW_MODAL,
    messageId: string,
    action: (messageId: string, text: string) => void,
    closeAction: () => void
}

interface HideModalAction {
    type: typeof HIDE_MODAL
}

interface FetchUsersAction {
    type: typeof FETCH_USERS,
    id_token: string
}

interface SaveUsersAction {
    type: typeof FETCH_USERS,
    userList: Array<User>
}

export type ChatActionTypes =
    FetchMessagesAction
    | SaveMessagesAction
    | SendMessageAction
    | EditMessageAction
    | DeleteMessageAction
    | ShowModalAction
    | HideModalAction
    | FetchUsersAction
    | SaveUsersAction;