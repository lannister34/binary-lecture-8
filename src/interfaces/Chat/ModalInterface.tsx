export interface ModalPropsInterface {
    isShown: boolean,
    title: string,
    value: string,
    action: (text: string) => void,
    closeAction: () => void
}

export interface ModalStateInterface {
    value: string,
    isValueSpecified: boolean
}