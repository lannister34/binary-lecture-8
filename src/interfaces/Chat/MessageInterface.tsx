import {ChatActionTypes} from "./ChatInterface";

export interface Message {
    id: string,
    text: string,
    userId: string,
    editedAt: string,
    createdAt: string
}

export interface MessageProps extends Message {
    isCurrentUser: boolean,
    avatar: string,
    username: string,
    showModal: (messageId: string, action: (messageId: string, text: string) => void, closeAction: () => void) => ChatActionTypes,
    hideModal: () => ChatActionTypes,
    editMessage: (message: Message) => ChatActionTypes,
    deleteMessage: (token_id: string, messageId: string) => ChatActionTypes,
    id_token: string
}

export interface MessageState {
    isLiked: boolean
}
