import Message from "../Message/MessageInterface";
import User from "../Chat/UserInterface";

interface MessageInputPropsInterface {
    currentUser: User | null
}

export default MessageInputPropsInterface;