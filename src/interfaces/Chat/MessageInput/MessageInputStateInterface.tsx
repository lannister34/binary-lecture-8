interface MessageInputState {
    value: string,
    isFocused: false
}

export default MessageInputState;