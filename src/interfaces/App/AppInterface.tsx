import {User} from "../Chat/ChatInterface";

export const LOGIN_REQUEST : any = "LOGIN_REQUEST";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILURE = "LOGIN_FAILURE";
export const COMPARE_SESSION_TOKEN = "COMPARE_SESSION_TOKEN";
export const FETCH = "FETCH";
export const FETCHED = "FETCHED";

export interface AppProps {
    app: AppState,
    compareSessionToken: () => AppActionTypes,
    requestLogin: (username: string, password: string) =>  AppActionTypes,
    receiveLogin: (user: User, id_token:string) =>  AppActionTypes,
    loginError: (message:string) =>  AppActionTypes
}

export interface AppState {
    isFetching: boolean,
    isAuthenticated: boolean,
    error_message: string,
    user: User,
    id_token: string
}

interface CompareSessionTokenAction {
    type: typeof COMPARE_SESSION_TOKEN
}

interface FetchAction {
    type: typeof FETCH
}

interface FetchedAction {
    type: typeof FETCHED
}

interface LoginRequestAction {
    type: typeof LOGIN_REQUEST,
    isFetching: boolean,
    isAuthenticated: boolean,
    username: string,
    password: string
}

interface LoginReceiveAction {
    type: typeof LOGIN_SUCCESS,
    isFetching: boolean,
    isAuthenticated: boolean,
    user: User,
    id_token: string
}

interface LoginErrorAction {
    type: typeof LOGIN_FAILURE,
    isFetching: false,
    isAuthenticated: false,
    message: string
}

export type AppActionTypes = FetchAction | FetchedAction | CompareSessionTokenAction | LoginRequestAction | LoginReceiveAction | LoginErrorAction;