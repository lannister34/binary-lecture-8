import {AppActionTypes} from "../App/AppInterface";

export const UPDATE_USERNAME: string = "UPDATE_USERNAME";
export const UPDATE_PASSWORD: string = "UPDATE_PASSWORD";

export interface LoginProps {
    login: LoginState,
    updateUsername: (value: string) => LoginActionTypes
    updatePassword: (value: string) => LoginActionTypes
    requestLogin: (username: string, password: string) =>  AppActionTypes,
    messageError: string
}

export interface LoginState {
    username: string,
    password: string
}

interface UpdateUsernameAction {
    type: typeof UPDATE_USERNAME,
    value: string
}

interface UpdatePasswordAction {
    type: typeof UPDATE_PASSWORD,
    value: string
}

export type LoginActionTypes = UpdateUsernameAction | UpdatePasswordAction;