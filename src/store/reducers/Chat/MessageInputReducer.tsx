import MessageInputState from "../../../interfaces/Chat/MessageInput/MessageInputStateInterface";
import {
    MessageInputActionTypes,
    UPDATE_VALUE,
    FOCUS_ON
} from "../../../interfaces/Chat/MessageInput/MessageInputActionInterface";

const initialState: MessageInputState = {
    value: "",
    isFocused: false
};

export function MessageInputReducer(state: MessageInputState = initialState, action: MessageInputActionTypes) {
    switch (action.type) {
        case UPDATE_VALUE:
            if (!("value" in action)) {
                return state;
            }
            return Object.assign({}, state, {
                value: action.value
            })
        case FOCUS_ON:
            return Object.assign({}, state, {
                isFocused: true
            })
        default:
            return state;
    }
}