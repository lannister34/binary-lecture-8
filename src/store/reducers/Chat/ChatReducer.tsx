import User from "../../../interfaces/Chat/Chat/UserInterface";
import Message from "../../../interfaces/Chat/Message/MessageInterface";
import ChatStateInterface from "../../../interfaces/Chat/Chat/ChatStateInterface";
import {
    ChatActionTypes,
    DELETE_MESSAGE,
    EDIT_MESSAGE,
    GET_MESSAGES,
    SEND_MESSAGE,
    SHOW_MODAL,
    HIDE_MODAL,
    UPDATE_USERS
} from "../../../interfaces/Chat/Chat/ChatActionInterface";

const initialState: ChatStateInterface = {
    currentUser: null,
    userList: new Map(),
    userCount: 23,
    messageList: [],
    messageCount: 0,
    lastMessageTime: null,
    isGotData: false,
    modalData: {
        isShown: false,
        title: "",
        value: "",
        action: () => {
        },
        closeAction: () => {
        }
    }
};

const createUserList: (messageList: Array<Message>) => Map<string, User> = (messageList: Array<Message>) => {
    const userList: Map<string, User> = new Map();

    messageList.forEach((message: Message) => {
        if (userList.has(message.userId)) {
            return;
        }

        userList.set(message.userId, {
            "userId": message.userId,
            "user": message.user,
            "avatar": message.avatar
        });
    });

    return userList;
}


export function ChatReducer(state: ChatStateInterface = initialState, action: ChatActionTypes): ChatStateInterface {
    switch (action.type) {
        case UPDATE_USERS:
            if (!("userList" in action)) {
                return state;
            }
            return Object.assign({}, state, {
                userList: action.userList
            })
        case GET_MESSAGES:
            if (!("messageList" in action)) {
                return state;
            }
            const userList: Map<string, User> = createUserList(action.messageList);
            const lastMessageIndex: number = action.messageList.length - 1;
            let lastMessageTime: string | null = null;
            if (action.messageList[lastMessageIndex]) {
                const date: Date = new Date(action.messageList[lastMessageIndex].createdAt);
                lastMessageTime = String(date.getHours()) + ":" + String(date.getMinutes());
            }
            return Object.assign({}, state, {
                messageList: action.messageList,
                currentUser: action.messageList[0],
                userList,
                userCount: userList.size,
                messageCount: action.messageList.length,
                lastMessageTime: lastMessageTime,
                isGotData: true
            })
        case SEND_MESSAGE:
            const newMessageList: Array<Message> = state.messageList;
            if ("message" in action) {
                newMessageList.push(action.message);
            }
            return Object.assign({}, state, {
                messageList: newMessageList,
                messageCount: newMessageList.length
            })
        case EDIT_MESSAGE:
            return Object.assign({}, state, {
                messageList: state.messageList.map((message: Message) => {
                    if ("message" in action && message.id === action.message.id) {
                        message = action.message;
                    }
                    return message;
                }),
                modalData: {
                    isShown: false,
                    title: "",
                    value: "",
                    action: () => {
                    },
                    closeAction: () => {
                    }
                }
            })
        case DELETE_MESSAGE:
            const messageList = state.messageList.filter(
                message => {
                    if (!("messageId" in action)) {
                        return true;
                    }
                    return message.id !== action.messageId
                }
            );
            return Object.assign({}, state, {
                messageList,
                messageCount: messageList.length
            })
        case SHOW_MODAL:
            if (!("messageId" in action && "action" in action)) {
                return state;
            }
            const message = state.messageList.find((message: Message) => message.id === action.messageId);

            if (!message) {
                return state;
            }

            return Object.assign({}, state, {
                modalData: {
                    isShown: true,
                    title: "Edit Message",
                    value: message.text,
                    action: action.action,
                    closeAction: action.closeAction
                }
            })
        case HIDE_MODAL:
            return Object.assign({}, state, {
                modalData: {
                    isShown: false,
                    title: "",
                    value: "",
                    action: () => {
                    },
                    closeAction: () => {
                    }
                }
            })
        default:
            return state;
    }
}