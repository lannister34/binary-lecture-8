import {combineReducers} from "redux";
import {ChatReducer} from "./Chat/ChatReducer";
import {MessageInputReducer} from "./Chat/MessageInputReducer";

const RootReducer = combineReducers({
    chat: ChatReducer,
    messageInput: MessageInputReducer
});

export default RootReducer;

export type RootState = ReturnType<typeof RootReducer>