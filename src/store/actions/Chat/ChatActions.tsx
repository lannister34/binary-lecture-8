import Message from "../../../interfaces/Chat/Message/MessageInterface";
import User from "../../../interfaces/Chat/Chat/UserInterface";
import {
    GET_MESSAGES,
    SEND_MESSAGE,
    EDIT_MESSAGE,
    DELETE_MESSAGE,
    SHOW_MODAL,
    HIDE_MODAL,
    ChatActionTypes, UPDATE_USERS,
} from "../../../interfaces/Chat/Chat/ChatActionInterface";


export function updateUsers(userList: Array<User>) {
    return {
        type: UPDATE_USERS,
        userList
    }
}

export function getMessages(messageList: Array<Message>): ChatActionTypes {
    return {
        type: GET_MESSAGES,
        messageList
    }
}

export function sendMessage(message: Message): ChatActionTypes {
    return {
        type: SEND_MESSAGE,
        message
    }
}

export function editMessage(message: Message): ChatActionTypes {
    return {
        type: EDIT_MESSAGE,
        message
    }
}

export function deleteMessage(messageId: string): ChatActionTypes {
    return {
        type: DELETE_MESSAGE,
        messageId
    }
}

export function showModal(messageId: string, action: (messageId: string, text: string) => void, closeAction: () => void): ChatActionTypes {
    return {
        type: SHOW_MODAL,
        messageId,
        action,
        closeAction
    }
}

export function hideModal(): ChatActionTypes {
    return {
        type: HIDE_MODAL
    }
}