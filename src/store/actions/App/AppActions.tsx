import {
    AppActionTypes,
    COMPARE_SESSION_TOKEN,
    FETCH, FETCHED,
    LOGIN_FAILURE,
    LOGIN_REQUEST,
    LOGIN_SUCCESS
} from "../../../interfaces/App/AppInterface";
import {User} from "../../../interfaces/Chat/ChatInterface";

export function compareSessionToken(): AppActionTypes {
    return {
        type: COMPARE_SESSION_TOKEN
    }
}

export function fetchData(): AppActionTypes {
    return {
        type: FETCH
    }
}

export function fetchedData(): AppActionTypes {
    return {
        type: FETCHED
    }
}

export function requestLogin(username: string, password: string): AppActionTypes {
    return {
        type: LOGIN_REQUEST,
        isFetching: true,
        isAuthenticated: false,
        username,
        password
    }
}

export function receiveLogin(user: User, id_token:string): AppActionTypes {
    return {
        type: LOGIN_SUCCESS,
        isFetching: false,
        isAuthenticated: true,
        user,
        id_token
    }
}

export function loginError(message:string): AppActionTypes {
    return {
        type: LOGIN_FAILURE,
        isFetching: false,
        isAuthenticated: false,
        message
    }
}