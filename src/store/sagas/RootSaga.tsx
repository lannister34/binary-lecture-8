import {call, put, takeEvery, takeLeading, all} from "redux-saga/effects";
import axios from "axios";
import {API_URL} from "../../config";
import {AppActionTypes, COMPARE_SESSION_TOKEN, LOGIN_REQUEST, LOGIN_SUCCESS} from "../../interfaces/App/AppInterface";
import {fetchData, fetchedData, loginError, receiveLogin} from "../actions/App/AppActions";
import {
    ChatActionTypes,
    DELETE_MESSAGE,
    FETCH_MESSAGES,
    FETCH_USERS,
    SEND_MESSAGE
} from "../../interfaces/Chat/ChatInterface";
import {saveMessages, saveUsers} from "../actions/Chat/ChatActions";

function* compareSessionToken() {
    yield put(fetchData());
    const id_token = localStorage.getItem('id_token');
    if (!id_token) {
        yield put(fetchedData());
        return;
    }
    const response = yield call(axios.post, `${API_URL}/session/compare`, {
        id_token
    });
    const responseData = response.data;
    if (responseData.error) {
        yield put(loginError(responseData.message));
        return;
    }
    if (responseData.isExist) {
        yield put(receiveLogin(responseData.user, id_token));
    } else {
        yield put(fetchedData());
    }
}

function* watchCompareSessionToken() {
    yield takeLeading(COMPARE_SESSION_TOKEN, compareSessionToken);
}

function* login(userData: AppActionTypes) {
    try {
        yield put(fetchData());
        const response = yield call(axios.post, `${API_URL}/login`, userData);
        const responseData = response.data;
        if (responseData.error) {
            yield put(loginError(responseData.message));
            return;
        }
        localStorage.setItem('id_token', responseData.id_token);
        yield put(receiveLogin(responseData.user, responseData.id_token));
    } catch (error) {
        yield put(loginError(error));
    }
}

function* watchLogin() {
    yield takeLeading(LOGIN_REQUEST, login);
}

function* fetchMessages(data: ChatActionTypes) {
    try {
        yield put(fetchData());
        if (!("id_token" in data)) {
            yield put(loginError("You need to log in."));
            return;
        }
        const response = yield call(axios.get, `${API_URL}/messages?id_token=${data.id_token}`);
        const responseData = response.data;
        if (responseData.error) {
            yield put(loginError(responseData.message));
            return;
        }
        yield put(saveMessages(responseData));
        yield put(fetchedData());
    } catch (error) {
        yield put(loginError(error));
    }
}

function* watchFetchMessages() {
    yield takeLeading(FETCH_MESSAGES, fetchMessages);
}

function* fetchUsers(data: ChatActionTypes) {
    try {
        yield put(fetchData());
        if (!("id_token" in data)) {
            yield put(loginError("You need to log in."));
            return;
        }
        const response = yield call(axios.get, `${API_URL}/users?id_token=${data.id_token}`);
        const responseData = response.data;
        if (responseData.error) {
            yield put(loginError(responseData.message));
            return;
        }
        yield put(saveUsers(responseData));
        yield put(fetchedData());
    } catch (error) {
        yield put(loginError(error));
    }
}

function* watchFetchUsers() {
    yield takeLeading(FETCH_USERS, fetchUsers);
}

function* sendMessage(data: ChatActionTypes) {
    try {
        yield put(fetchData());
        if (!("id_token" in data && "message" in data)) {
            yield put(loginError("You need to log in."));
            return;
        }
        const response = yield call(axios.post, `${API_URL}/messages`, {
            id_token: data.id_token,
            message: data.message
        });

        const responseData = response.data;
        if (responseData.error) {
            yield put(loginError(responseData.message));
            return;
        }
        yield put(saveMessages(responseData));
        yield put(fetchedData());
    } catch (error) {
        //yield put(loginError(error));
    }
}

function* watchSendMessage() {
    yield takeLeading(SEND_MESSAGE, sendMessage);
}

function* deleteMessage(data: ChatActionTypes) {
    try {
        yield put(fetchData());
        if (!("id_token" in data && "messageId" in data)) {
            yield put(loginError("You need to log in."));
            return;
        }
        const response = yield call(axios.delete, `${API_URL}/messages?id_token=${data.id_token}&message_id=${data.messageId}`);

        const responseData = response.data;
        if (responseData.error) {
            yield put(loginError(responseData.message));
            return;
        }
        yield put(saveMessages(responseData));
        yield put(fetchedData());
    } catch (error) {
        //yield put(loginError(error));
    }
}

function* watchDeleteMessage() {
    yield takeLeading(DELETE_MESSAGE, deleteMessage);
}
export default function* RootSaga() {
    yield all([
        watchCompareSessionToken(),
        watchLogin(),
        watchFetchMessages(),
        watchFetchUsers(),
        watchSendMessage(),
        watchDeleteMessage()
    ])
};