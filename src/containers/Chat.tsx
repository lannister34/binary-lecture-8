import React, {Component} from "react";
import {connect} from "react-redux";
import Header from "../components/Chat/Header/Header";
import MessageList from "../components/Chat/MessageList/MessageList";
import MessageInput from "../components/Chat/MessageInput/MessageInput";
import LoadingSpinner from "../components/LoadingSpinner/LoadingSpinner";
import ChatProps from "../interfaces/Chat/Chat/ChatPropsInterface";
import ChatState from "../interfaces/Chat/Chat/ChatStateInterface";
import {RootState} from "../store/reducers/RootReducer";
import * as ChatActions from "../store/actions/Chat/ChatActions";
import Message from "../interfaces/Chat/Message/MessageInterface";
import store from "../store/store";
import messages from "../messages";
import "../styles/chat.css";
import Modal from "../components/Chat/Modal/Modal";
import MessageProps from "../interfaces/Chat/Message/MessagePropsInterface";


class Chat extends Component<ChatProps, ChatState> {
    private API_LINK: string = "https://edikdolynskyi.github.io/react_sources/messages.json";
    private FAKE_CALL_API: boolean = true;

    async componentDidMount() {
        const messageList: Array<Message> = await this.fetchMessages();
        const sortedMessageList: Array<Message> = this.sortMessages(messageList);
        const extendedMessageList: Array<MessageProps> = sortedMessageList.map((message: Message) => {
            return Object.assign({}, message, {
                isCurrentUser: false,
                isLiked: false
            });
        })

        store.dispatch(ChatActions.getMessages(extendedMessageList));
    }

    async fetchMessages() {
        if (this.FAKE_CALL_API) {
            return messages;
        }
        return fetch(this.API_LINK + "?id=" + this.props.chatId)
            .then((response: Response) => (response.ok ? response.json() : []))
    }

    sortMessages(messageList: Array<Message>) {
        return messageList.sort((prev: Message, curr: Message) => {
            const prevDate: number = new Date(prev.createdAt).getTime();
            const currDate: number = new Date(curr.createdAt).getTime();
            return prevDate - currDate;
        })
    }

    render() {
        const state = store.getState().chat;
        let content: Array<JSX.Element> | JSX.Element;

        if (state.isGotData) {
            content = [
                <div className="chat__wrapper container" key={this.props.chatId}>
                    <Header id={this.props.chatId} name={this.props.chatName} userCount={state.userCount}
                            messageCount={state.messageCount} lastMessageTime={state.lastMessageTime}/>
                    <MessageList currentUser={state.currentUser} messageList={state.messageList}/>
                    <MessageInput currentUser={state.currentUser}/>
                </div>,
                <Modal isShown={state.modalData.isShown} title={state.modalData.title} value={state.modalData.value}
                       action={state.modalData.action} closeAction={state.modalData.closeAction}/>
            ];

        } else {
            content = <LoadingSpinner/>
        }

        return (
            content
        );
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        chat: state.chat
    }
}

const mapDispatchToProps = ChatActions;

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
