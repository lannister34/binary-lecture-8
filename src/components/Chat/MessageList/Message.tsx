import React, {Component} from "react";
import PropTypes from "prop-types";
import MessageProps from "../../../interfaces/Chat/Message/MessagePropsInterface";
import MessageState from "../../../interfaces/Chat/Message/MessageStateInterface";
import MessageInterface from "../../../interfaces/Chat/Message/MessageInterface";
import "../../../styles/message.css";
import store from "../../../store/store";
import * as ChatActions from "../../../store/actions/Chat/ChatActions";

class Message extends Component<MessageProps, MessageState> {
    constructor(props: MessageProps) {
        super(props);
        this.state = {
            isLiked: false
        }
    }

    static propTypes = {
        id: PropTypes.string,
        text: PropTypes.string,
        user: PropTypes.string,
        avatar: PropTypes.string,
        userId: PropTypes.string,
        editedAt: PropTypes.string,
        createdAt: PropTypes.string,
        isLiked: PropTypes.bool
    }

    formatDate() {
        const date: Date = new Date(this.props.createdAt);
        return String(date.getHours()) + ":" + String(date.getMinutes());
    }

    editMessage = (event: React.MouseEvent) => {
        store.dispatch(ChatActions.showModal(this.props.id, this.updateMessage, this.hideModal));
    }

    updateMessage = (text: string) => {
        const message: MessageInterface = {
            id: this.props.id,
            text,
            user: this.props.user,
            avatar: this.props.avatar,
            userId: this.props.userId,
            editedAt: (new Date()).toISOString(),
            createdAt: this.props.createdAt
        };
        store.dispatch(ChatActions.editMessage(message));
        this.hideModal();
    }

    deleteMessage = (event: React.MouseEvent) => {
        store.dispatch(ChatActions.deleteMessage(this.props.id));
    }

    likeMessage = (event: React.MouseEvent) => {
        this.setState((prevState: MessageState) => {
            return {
                isLiked: !prevState.isLiked
            }
        })
    }

    hideModal() {
        store.dispatch(ChatActions.hideModal());
    }

    render() {
        const actions: Array<JSX.Element> | JSX.Element = this.props.isCurrentUser ? [
            <span onClick={this.editMessage} className="message__icon message__edit-icon"/>,
            <span onClick={this.deleteMessage} className="message__icon message__delete-icon"/>
        ] : <span onClick={this.likeMessage}
                  className={"message__icon message__like-icon" + (this.state.isLiked ? " active" : "")}/>;

        return (
            <div className="messages__message" key={this.props.id}>
                <div className="message__group">
                    <div className="message__avatar-block">
                        <img alt="avatar" className="message__avatar" src={this.props.avatar}/>
                    </div>
                    <div className="message__header">
                        <span className="message__user-name">{this.props.user}</span>
                        <span className="message__time">{this.formatDate()}</span>
                    </div>
                    <div className="message__content-block">
                        <div className="message__content">{this.props.text}</div>
                    </div>
                </div>
                <div className="message__actions">
                    {actions}
                </div>
            </div>
        )
    }
}

export default Message;