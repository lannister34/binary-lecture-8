import React, {Component} from "react";
import PropTypes from "prop-types";
import ModalPropsInterface from "../../../interfaces/Chat/Modal/ModalPropsInterface";
import ModalStateInterface from "../../../interfaces/Chat/Modal/ModalStateInterface";

class Modal extends Component<ModalPropsInterface, ModalStateInterface> {
    constructor(props: ModalPropsInterface) {
        super(props);
        this.state = {
            "value": "",
            "isValueSpecified": false
        }
    }
    static propTypes = {
        isShown: PropTypes.bool,
        title: PropTypes.string,
        value: PropTypes.string,
        action: PropTypes.func,
        closeAction: PropTypes.func
    }

    static defaultProps = {
        isShown: false,
        title: "",
        value: "",
        action: () => {
        },
        closeAction: () => {
        }
    }

    componentDidUpdate() {
        this.props.value && !this.state.isValueSpecified && (
            this.setState(() => {
                return {
                    value: this.props.value,
                    isValueSpecified: true
                }
            })
        )
    }

    handleChange = (event: React.FormEvent) => {
        const target: HTMLInputElement = event.target as HTMLInputElement;
        target.value !== this.state.value && (
            this.setState(() => {
                return {
                    value: target.value
                }
            })
        )
    }

    successAction = () => {
        this.setState(() => {
            return {
                value: "",
                isValueSpecified: false
            }
        });
        this.props.action(this.state.value);
    }

    closeAction = () => {
        this.setState(() => {
            return {
                value: "",
                isValueSpecified: false
            }
        });
        this.props.closeAction();
    }

    renderModal() {
        return [
            <div className="modal d-block" tabIndex={-1} role="dialog">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">{this.props.title}</h5>
                            <button onClick={this.closeAction} type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <input onChange={this.handleChange} className="form-control" type="text" value={this.state.value} />
                        </div>
                        <div className="modal-footer">
                            <button onClick={this.successAction} type="button" className="btn btn-primary">Save</button>
                            <button onClick={this.closeAction} type="button" className="btn btn-secondary"
                                    data-dismiss="modal">Close
                            </button>
                        </div>
                    </div>
                </div>
            </div>,
            <div className="modal-backdrop fade show"/>
        ];
    }

    render() {
        return this.props.isShown ? this.renderModal() : null;
    }
}

export default Modal;