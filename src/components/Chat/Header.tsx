import React, {Component} from "react";
import {HeaderProps, HeaderState} from "../../interfaces/Chat/HeaderInterface";
import "../../styles/header.css";

class Header extends Component<HeaderProps, HeaderState> {

    render() {
        const lastMessage = this.props.lastMessageTime ? `last message at ${this.props.lastMessageTime}` : "";
        return (
            <div className="chat__header">
                <div className="header__wrapper">
                    <div className="header__info">
                        <div className="header__name">{this.props.name}</div>
                        <div className="header__participants-count">{this.props.userCount} participants</div>
                        <div
                            className="header__messages-count">{this.props.messageCount ? this.props.messageCount + " messages" : ""}</div>
                    </div>
                    <div className="header__last-message">{lastMessage}</div>
                </div>
            </div>
        )
    }
}

export default Header;
