import React, {Component} from "react";
import PropTypes from "prop-types";

export default class LoadingSpinner extends Component<any, any> {

    constructor(props:any) {
        super(props);
    }

    static propTypes = {
        height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
        color: PropTypes.string,
        radius: PropTypes.number
    };

    static defaultProps = {
        height: 80,
        width: 80,
        color: "#00BFFF",
        radius: 6,
    }

    render() {
        return (
            <div className="loader-spinner">
                <div className="modal-backdrop fade show"> </div>
                <svg
                    className="spinner"
                    width={this.props.width}
                    height={this.props.height}
                    viewBox="0 0 38 38"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <defs>
                        <linearGradient x1="8.042%" y1="0%" x2="65.682%" y2="23.865%" id="a">
                            <stop stopColor={this.props.color} stopOpacity="0" offset="0%" />
                            <stop stopColor={this.props.color} stopOpacity=".631" offset="63.146%" />
                            <stop stopColor={this.props.color} offset="100%" />
                        </linearGradient>
                    </defs>
                    <g fill="none" fillRule="evenodd">
                        <g transform="translate(1 1)">
                            <path d="M36 18c0-9.94-8.06-18-18-18" id="Oval-2" stroke={this.props.color} strokeWidth="2">
                                <animateTransform
                                    attributeName="transform"
                                    type="rotate"
                                    from="0 18 18"
                                    to="360 18 18"
                                    dur="0.9s"
                                    repeatCount="indefinite"
                                />
                            </path>
                        </g>
                    </g>
                </svg>
            </div>
        );
    }
}
