import React, {Component} from "react";
import store from "../../store/store";
import * as LoginActions from "../../store/actions/Login/LoginActions";
import {LoginProps, LoginState} from "../../interfaces/Login/LoginInterface";
import {RootState} from "../../store/reducers/RootReducer";
import "../../styles/login.css";
import {connect} from "react-redux";

class Login extends Component<LoginProps, LoginState> {

    constructor(props: LoginProps) {
        super(props);
        this.usernameChangeHandler = this.usernameChangeHandler.bind(this);
        this.passwordChangeHandler = this.passwordChangeHandler.bind(this);
        this.clickHandler = this.clickHandler.bind(this);
    }

    usernameChangeHandler(event: React.FormEvent<HTMLInputElement>) {
        const target: HTMLInputElement = event.target as HTMLInputElement;
        target.value !== this.props.login.username && (
            this.props.updateUsername(target.value)
        )
    }

    passwordChangeHandler(event: React.FormEvent<HTMLInputElement>) {
        const target: HTMLInputElement = event.target as HTMLInputElement;
        target.value !== this.props.login.password && (
            this.props.updatePassword(target.value)
        )
    }

    clickHandler(event: React.MouseEvent) {
        event.preventDefault();
        const state = this.props.login;
        if (state.username && state.password) {
            this.props.requestLogin(state.username, state.password);
        }
    }

    render() {
        const errorBox = (
            <div className="alert alert-danger" role="alert">
                {this.props.messageError}
            </div>
        );
        return (
            <main className="login-form">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-md-8">
                            <div className="card">
                                {this.props.messageError ? errorBox : null}
                                <div className="card-header">Authorization</div>
                                <div className="card-body">
                                    <form method="POST">
                                        <div className="form-group row">
                                            <label
                                                className="col-md-4 col-form-label text-md-right">Username</label>
                                            <div className="col-md-6">
                                                <input onChange={this.usernameChangeHandler} type="text" id="username" className="form-control" name="username"
                                                       required/>
                                            </div>
                                        </div>

                                        <div className="form-group row">
                                            <label className="col-md-4 col-form-label text-md-right">Password</label>
                                            <div className="col-md-6">
                                                <input onChange={this.passwordChangeHandler} type="password" id="password" className="form-control"
                                                       name="password" required/>
                                            </div>
                                        </div>

                                        <div className="col-md-6 offset-md-4">
                                            <button onClick={this.clickHandler} type="submit" className="btn btn-primary">
                                                Log in
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        );
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        login: state.login
    }
}

const mapDispatchToProps = LoginActions;

export default connect(mapStateToProps, mapDispatchToProps)(Login);
