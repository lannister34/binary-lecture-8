import React, {Component} from "react";
import PropTypes from "prop-types";
import {MessageInputProps, MessageInputState} from "../../interfaces/Chat/MessageInputInterface";
import * as MessageInputActions from "../../store/actions/Chat/MessageInputActions";
import "../../styles/messageInput.css";
import {Message} from "../../interfaces/Chat/MessageInterface";
import store from "../../store/store";
import {User} from "../../interfaces/Chat/ChatInterface";
import {sendMessage} from "../../store/actions/Chat/ChatActions";
import {connect} from "react-redux";
import {RootState} from "../../store/reducers/RootReducer";
import * as ChatActions from "../../store/actions/Chat/ChatActions";

class MessageInput extends Component<MessageInputProps, MessageInputState> {

    componentDidMount() {
        window.addEventListener('keydown', this.handleKeyPress);
    }

    componentWillUnmount() {
        window.removeEventListener('keydown', this.handleKeyPress);
    }

    handleKeyPress = (event: KeyboardEvent) => {
        if (event.code === "Enter") {
            const state: MessageInputState = store.getState().messageInput;
            if (state.isFocused) this.handleClick();
        }
    }

    handleChange = (event: React.FormEvent<HTMLInputElement>) => {
        const target: HTMLInputElement = event.target as HTMLInputElement;
        target.value !== store.getState().messageInput.value && (
            this.props.updateValue(target.value)
        )
    }

    handleClick = () => {
        const user: User | null = this.props.currentUser;
        const state: MessageInputState = store.getState().messageInput;
        const emptyStringRegexp = new RegExp(/^\s+$/);
        if (!(user && state.value && !emptyStringRegexp.test(state.value))) {
            return;
        }
        const message: Message = {
            id: "",
            text: state.value,
            userId: user.id,
            editedAt: "",
            createdAt: (new Date()).toISOString()
        };

        this.props.updateValue("");
        this.props.sendMessage(this.props.id_token, message);
    }

    handleFocus = () => {
        !store.getState().messageInput.isFocused && (
            this.props.focusOn()
        );
    }

    handleBlur = () => {
        store.getState().messageInput.isFocused && (
            this.props.focusOff()
        );
    }

    render() {
        const state = this.props.messageInput;
        return (
            <div className="chat__message-input">
                <div className="message-input__input-block">
                    <input onFocus={this.handleFocus} onBlur={this.handleBlur} onChange={this.handleChange}
                           value={state.value} type="text" placeholder="Message"/>
                </div>
                <button onClick={this.handleClick} className="message-input__button"/>
            </div>
        );
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        messageInput: state.messageInput
    }
}

const mapDispatchToProps = MessageInputActions;

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);