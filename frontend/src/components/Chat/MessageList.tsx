import React, {Component} from "react";
import PropTypes from "prop-types";
import Message from "./Message";
import {MessageListProps, MessageListState} from "../../interfaces/Chat/MessageListInterface";
import {MessageProps} from "../../interfaces/Chat/MessageInterface";
import "../../styles/messageList.css";

class MessageList extends Component<MessageListProps, MessageListState> {
    private messageList: HTMLElement | undefined;
    private readonly messageListRef: (instance: HTMLDivElement) => void;

    constructor(props: MessageListProps) {
        super(props);
        this.messageListRef = (element: HTMLElement) => {
            this.messageList = element
        }
        this.state = {
            messageList: props.messageList.slice()
        }
    }

    static propTypes = {
        messageList: PropTypes.array,
        currentUserId: PropTypes.string,
        sendMessage: PropTypes.func
    }

    componentWillMount() {
        window.addEventListener("keydown", this.handleKeyPress);
    }

    componentWillUnmount() {
        window.removeEventListener("keydown", this.handleKeyPress);
    }

    componentDidUpdate() {
        if (this.messageList && this.props.messageList.length !== this.state.messageList.length) {
            const rect: ClientRect = this.messageList.getBoundingClientRect();
            this.messageList.scrollTo(0, rect.top + this.messageList.scrollHeight);
            this.setState(() => {
                return {
                    messageList: this.props.messageList.slice()
                }
            })
        }
    }

    handleKeyPress = (event: KeyboardEvent) => {
        if (event.code === "ArrowUp") {
            this.editMessage();
        }
    }

    editMessage = () => {
        const message: MessageProps | null = this.getLastMessage();
        if (!message) {
            return;
        }
        this.props.showModal(message.id, this.updateMessage, this.hideModal);
    }

    updateMessage = (text: string) => {
        const message: MessageProps | null = this.getLastMessage();
        if (!message) {
            return;
        }
        message.text = text;
        this.props.editMessage(message);
        this.hideModal();
    }

    getLastMessage() {
        if (!this.props.currentUser) {
            return null;
        }
        const reversedMessageList: Array<MessageProps> = this.props.messageList.slice().reverse();

        for (let i = 0; i < reversedMessageList.length; i++) {
            if (reversedMessageList[i].userId === this.props.currentUser.id) {
                return reversedMessageList[i];
            }
        }
        return null;
    }

    hideModal() {
        this.props.hideModal();
    }

    render() {

        return (
            <div ref={this.messageListRef} className="chat__message-list">
                {
                    this.props.messageList.map((message: MessageProps, index: Number, array: Array<MessageProps>) => {
                        let isCurrentUser: boolean = false;
                        let separatorValue: string | null;
                        if (this.props.currentUser && this.props.currentUser.id === message.userId) {
                            isCurrentUser = true;
                        }
                        const user = this.props.userList.get(message.userId);
                        if (!user) {
                            return;
                        }
                        return (
                            <Message key={message.id} id={message.id} avatar={user.avatar}
                                     createdAt={message.createdAt}
                                     editedAt={message.editedAt} text={message.text} username={user.username}
                                     userId={message.userId}
                                     isCurrentUser={isCurrentUser}
                                     showModal={this.props.showModal}
                                     hideModal={this.props.hideModal}
                                     editMessage={this.props.editMessage}
                                     deleteMessage={this.props.deleteMessage}
                                     id_token={this.props.id_token}
                            />
                        )
                    })
                }
            </div>
        );
    }
}

export default MessageList;
