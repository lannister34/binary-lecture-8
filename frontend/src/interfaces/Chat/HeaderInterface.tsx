export interface HeaderProps {
    name: string,
    userCount: number,
    messageCount: number,
    lastMessageTime: string | null
}

export interface HeaderState {

}
