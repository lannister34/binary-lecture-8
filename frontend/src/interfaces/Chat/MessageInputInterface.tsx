import {User} from "./ChatInterface";
import {sendMessage} from "../../store/actions/Chat/ChatActions";
import {Message} from "./MessageInterface";

export const UPDATE_VALUE: string = "UPDATE_VALUE";
export const FOCUS_ON: string = "FOCUS_ON";
export const FOCUS_OFF: string = "FOCUS_OFF";

export interface MessageInputProps {
    messageInput: MessageInputState,
    id_token: string,
    currentUser: User | null,
    updateValue: (value: string) => void,
    sendMessage: (id_token: string, message: Message) => void,
    focusOn: () => MessageInputActionTypes,
    focusOff: () => MessageInputActionTypes
}

export interface MessageInputState {
    value: string,
    isFocused: false
}

interface UpdateValueAction {
    type: typeof UPDATE_VALUE,
    value: string
}

interface FocusOnAction {
    type: typeof FOCUS_ON
}

interface FocusOFFAction {
    type: typeof FOCUS_OFF
}

export type MessageInputActionTypes = UpdateValueAction | FocusOnAction | FocusOFFAction;