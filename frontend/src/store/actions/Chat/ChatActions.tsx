import {Message, MessageProps} from "../../../interfaces/Chat/MessageInterface";
import {
    FETCH_MESSAGES,
    FETCH_USERS,
    SAVE_MESSAGES,
    SEND_MESSAGE,
    EDIT_MESSAGE,
    DELETE_MESSAGE,
    SHOW_MODAL,
    HIDE_MODAL,
    ChatActionTypes, User, SAVE_USERS,
} from "../../../interfaces/Chat/ChatInterface";

export function fetchUsers(id_token: string): ChatActionTypes {
    return {
        type: FETCH_USERS,
        id_token
    }
}

export function saveUsers(userList: Array<User>): ChatActionTypes {
    return {
        type: SAVE_USERS,
        userList
    }
}

export function fetchMessages(id_token: string): ChatActionTypes {
    return {
        type: FETCH_MESSAGES,
        id_token
    }
}

export function saveMessages(messageList: Array<Message>): ChatActionTypes {
    return {
        type: SAVE_MESSAGES,
        messageList
    }
}

export function sendMessage(id_token: string, message: Message): ChatActionTypes {
    return {
        type: SEND_MESSAGE,
        id_token,
        message
    }
}

export function editMessage(message: Message): ChatActionTypes {
    return {
        type: EDIT_MESSAGE,
        message
    }
}

export function deleteMessage(id_token: string, messageId: string): ChatActionTypes {
    return {
        type: DELETE_MESSAGE,
        id_token,
        messageId
    }
}

export function showModal(messageId: string, action: (messageId: string, text: string) => void, closeAction: () => void): ChatActionTypes {
    return {
        type: SHOW_MODAL,
        messageId,
        action,
        closeAction
    }
}

export function hideModal(): ChatActionTypes {
    return {
        type: HIDE_MODAL
    }
}