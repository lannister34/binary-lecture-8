import {
    FOCUS_OFF,
    FOCUS_ON,
    UPDATE_VALUE
} from "../../../interfaces/Chat/MessageInputInterface";


export function updateValue(value: string) {
    return {
        type: UPDATE_VALUE,
        value
    }
}

export function focusOn() {
    return {
        type: FOCUS_ON
    }
}

export function focusOff() {
    return {
        type: FOCUS_OFF
    }
}