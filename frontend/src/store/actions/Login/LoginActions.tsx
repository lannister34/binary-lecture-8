import {LoginActionTypes, UPDATE_PASSWORD, UPDATE_USERNAME} from "../../../interfaces/Login/LoginInterface";


export function updateUsername(value: string): LoginActionTypes {
    return {
        type: UPDATE_USERNAME,
        value
    }
}

export function updatePassword(value: string): LoginActionTypes {
    return {
        type: UPDATE_PASSWORD,
        value
    }
}