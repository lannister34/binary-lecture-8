import {
    AppActionTypes,
    AppState, FETCH, FETCHED,
    LOGIN_FAILURE,
    LOGIN_SUCCESS
} from "../../interfaces/App/AppInterface";

const initialState: AppState = {
    isFetching: false,
    isAuthenticated: false,
    error_message: "",
    user: {
        id: "",
        username: "",
        avatar: "",
        admin: false
    },
    id_token: "",
};

export function AppReducer(state: AppState = initialState, action: AppActionTypes) {
    switch (action.type) {
        case FETCH:
            return Object.assign({}, state, {
                isFetching: true
            })
        case FETCHED:
            return Object.assign({}, state, {
                isFetching: false
            })
        case LOGIN_SUCCESS:
            if (!("id_token" in action && "user" in action)) {
                return state;
            }
        return Object.assign({}, state, {
            isFetching: action.isFetching,
            isAuthenticated: action.isAuthenticated,
            user: action.user,
            id_token: action.id_token
        })
        case LOGIN_FAILURE:
            if (!("message" in action)) {
                return state;
            }
            return Object.assign({}, state, {
            isFetching: action.isFetching,
            isAuthenticated: action.isAuthenticated,
            error_message: action.message
        })
        default:
            return state;
    }
}