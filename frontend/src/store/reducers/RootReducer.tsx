import {combineReducers} from "redux";
import {ChatReducer} from "./Chat/ChatReducer";
import {MessageInputReducer} from "./Chat/MessageInputReducer";
import {LoginReducer} from "./Login/LoginReducer";
import {AppReducer} from "./AppReducer";

const RootReducer = combineReducers({
    app: AppReducer,
    chat: ChatReducer,
    messageInput: MessageInputReducer,
    login: LoginReducer
});

export default RootReducer;

export type RootState = ReturnType<typeof RootReducer>