import {LoginState} from "../../../interfaces/Login/LoginInterface";
import {LoginActionTypes, UPDATE_PASSWORD, UPDATE_USERNAME} from "../../../interfaces/Login/LoginInterface";

const initialState: LoginState = {
    "username": "",
    "password": ""
}

export function LoginReducer(state: LoginState = initialState, action: LoginActionTypes) {
    switch (action.type) {
        case UPDATE_USERNAME:
            if (!("value" in action)) {
                return state;
            }
            return Object.assign({}, state, {
                username: action.value
            })
        case UPDATE_PASSWORD:
            if (!("value" in action)) {
                return state;
            }
            return Object.assign({}, state, {
                password: action.value
            })
        default:
            return state;
    }
}