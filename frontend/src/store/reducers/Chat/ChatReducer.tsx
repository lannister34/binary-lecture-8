import {User, ChatState, SAVE_USERS} from "../../../interfaces/Chat/ChatInterface";
import {Message} from "../../../interfaces/Chat/MessageInterface";

import {
    ChatActionTypes,
    DELETE_MESSAGE,
    EDIT_MESSAGE,
    SAVE_MESSAGES,
    SEND_MESSAGE,
    SHOW_MODAL,
    HIDE_MODAL
} from "../../../interfaces/Chat/ChatInterface";

const initialState: ChatState = {
    currentUser: null,
    userList: new Map(),
    userCount: 0,
    messageList: [],
    messageCount: 0,
    lastMessageTime: null,
    modalData: {
        isShown: false,
        title: "",
        value: "",
        action: () => {
        },
        closeAction: () => {
        }
    }
};


export function ChatReducer(state: ChatState = initialState, action: ChatActionTypes): ChatState {
    switch (action.type) {
        case SAVE_USERS:
            if (!("userList" in action)) {
                return state;
            }
            const users = new Map();
            action.userList.forEach((user) => {
                users.set(user.id, user);
            });
            return Object.assign({}, state, {
                userCount: users.size,
                userList: users
            })
        case SAVE_MESSAGES:
            if (!("messageList" in action)) {
                return state;
            }
            const lastMessageIndex: number = action.messageList.length - 1;
            let lastMessageTime: string | null = null;
            if (action.messageList[lastMessageIndex]) {
                const date: Date = new Date(action.messageList[lastMessageIndex].createdAt);
                lastMessageTime = String(date.getHours()) + ":" + String(date.getMinutes());
            }
            return Object.assign({}, state, {
                messageList: action.messageList,
                messageCount: action.messageList.length,
                lastMessageTime: lastMessageTime
            })
        case SEND_MESSAGE:
            const newMessageList: Array<Message> = state.messageList;
            if ("message" in action) {
                newMessageList.push(action.message);
            }
            return Object.assign({}, state, {
                messageList: newMessageList,
                messageCount: newMessageList.length
            })
        case EDIT_MESSAGE:
            return Object.assign({}, state, {
                messageList: state.messageList.map((message: Message) => {
                    if ("message" in action && message.id === action.message.id) {
                        message = action.message;
                    }
                    return message;
                }),
                modalData: {
                    isShown: false,
                    title: "",
                    value: "",
                    action: () => {
                    },
                    closeAction: () => {
                    }
                }
            })
        case SHOW_MODAL:
            if (!("messageId" in action && "action" in action)) {
                return state;
            }
            const message = state.messageList.find((message: Message) => message.id === action.messageId);

            if (!message) {
                return state;
            }

            return Object.assign({}, state, {
                modalData: {
                    isShown: true,
                    title: "Edit Message",
                    value: message.text,
                    action: action.action,
                    closeAction: action.closeAction
                }
            })
        case HIDE_MODAL:
            return Object.assign({}, state, {
                modalData: {
                    isShown: false,
                    title: "",
                    value: "",
                    action: () => {
                    },
                    closeAction: () => {
                    }
                }
            })
        default:
            return state;
    }
}