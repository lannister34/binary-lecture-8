import React, {Component} from "react";
import {connect} from "react-redux";
import Header from "../components/Chat/Header";
import MessageList from "../components/Chat/MessageList";
import MessageInput from "../components/Chat/MessageInput";
import LoadingSpinner from "../components/LoadingSpinner/LoadingSpinner";
import {ChatProps, ChatState} from "../interfaces/Chat/ChatInterface";
import {RootState} from "../store/reducers/RootReducer";
import * as ChatActions from "../store/actions/Chat/ChatActions";
import {Message} from "../interfaces/Chat/MessageInterface";
import store from "../store/store";
import messages from "../messages";
import "../styles/chat.css";
import Modal from "../components/Chat/Modal";

class Chat extends Component<ChatProps, ChatState> {
    private API_LINK: string = "https://edikdolynskyi.github.io/react_sources/messages.json";
    private FAKE_CALL_API: boolean = true;

    async componentDidMount() {
        await this.props.fetchUsers(this.props.id_token);
        await this.props.fetchMessages(this.props.id_token);
    }

    sortMessages(messageList: Array<Message>) {
        return messageList.sort((prev: Message, curr: Message) => {
            const prevDate: number = new Date(prev.createdAt).getTime();
            const currDate: number = new Date(curr.createdAt).getTime();
            return prevDate - currDate;
        })
    }

    render() {
        const state = this.props.chat;

        return [
            <div className="chat__wrapper container">
                <Header name="Casino White Royal" userCount={state.userCount}
                        messageCount={state.messageCount} lastMessageTime={state.lastMessageTime}/>
                <MessageList editMessage={this.props.editMessage} deleteMessage={this.props.deleteMessage}
                             currentUser={this.props.currentUser}
                             userList={state.userList} messageList={state.messageList} showModal={this.props.showModal}
                             hideModal={this.props.hideModal} id_token={this.props.id_token}/>
                <MessageInput id_token={this.props.id_token} currentUser={this.props.currentUser}
                              sendMessage={this.props.sendMessage}/>
            </div>,
            <Modal isShown={state.modalData.isShown} title={state.modalData.title} value={state.modalData.value}
                   action={state.modalData.action} closeAction={state.modalData.closeAction}/>
        ]
    }
}

const mapStateToProps = (state: RootState) => {
    return {
        chat: state.chat
    }
}

const mapDispatchToProps = ChatActions;

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
