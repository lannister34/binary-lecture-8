import React, {Component} from "react";
import "./footer.css";

class Footer extends Component<any, any> {

    render() {
        return (
            <footer key="footer" className="footer bg-dark">
                <div className="footer-copyright text-center py-3">© 2020 Copyright:
                    <a href="https://mdbootstrap.com/"> Binary Studio</a>
                </div>
            </footer>
        )
    }
}

export default Footer;