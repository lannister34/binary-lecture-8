import React, {Component} from "react";
import Chat from "./containers/Chat";
import Header from './page/Header/Header';
import Footer from "./page/Footer/Footer";
import LoadingSpinner from "./components/LoadingSpinner/LoadingSpinner";
import Login from "./components/Login/Login";
import {AppProps, AppState} from "./interfaces/App/AppInterface";
import {RootState} from "./store/reducers/RootReducer";
import * as AppActions from "./store/actions/App/AppActions";
import {connect} from "react-redux";

class App extends Component<AppProps, AppState> {
    constructor(props: AppProps) {
        super(props);
    }

    componentDidMount() {
        this.props.compareSessionToken();
    }

    render() {
        const loadingSpinner = this.props.app.isFetching ? <LoadingSpinner/> : null;
        const main = this.props.app.isAuthenticated ? <Chat currentUser={this.props.app.user} id_token={this.props.app.id_token}/> :
            <Login messageError={this.props.app.error_message} requestLogin={this.props.requestLogin}/>
        return [
            loadingSpinner,
            <Header/>,
            main,
            <Footer/>
        ];
    }
}


const mapStateToProps = (state: RootState) => {
    return {
        app: state.app
    }
}

const mapDispatchToProps = AppActions;

export default connect(mapStateToProps, mapDispatchToProps)(App);
