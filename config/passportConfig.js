import {Strategy as LocalStrategy} from 'passport-local';
import bcrypt from 'bcrypt';
import db from '../data/db/dbAdapter';

function initialize(passport) {
  passport.use(new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
  }, (username, password, done) => {
    const user = db.getData(`/users/${username}`);

    if (!user) {
      return done(null, false,
          {message: 'Account with this username doesn\'t exist.'});
    }

    bcrypt.compare(password, user.password, (error, isMatch) => {
      if (error) {
        throw error;
      }

      if (isMatch) {
        return done(null, user);
      } else {
        return done(null, false, {message: 'Wrong password.'});
      }
    });
  }));

  passport.serializeUser((user, done) => done(null, user.username));
  passport.deserializeUser((username, done) => {
    const user = db.getData(`/users/${username}`);

    return done(null, user);
  });
}

module.exports = initialize;